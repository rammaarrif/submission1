package com.raivstudio.footballclub

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_club.view.*

class ClubAdapter (private val context: Context, private val items: List<ItemClub>, private val listener: (ItemClub) -> Unit)
    : RecyclerView.Adapter<ClubAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_club, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position], listener)
    }

    override fun getItemCount(): Int = items.size

    class ViewHolder (view : View) : RecyclerView.ViewHolder(view){

        fun bindItem(items: ItemClub, listener: (ItemClub) -> Unit) {
            itemView.namatim.text = items.name
            Glide.with(itemView.context).load(items.image).into(itemView.gambartim)
            itemView.setOnClickListener {
                listener(items)
            }
        }
    }
}