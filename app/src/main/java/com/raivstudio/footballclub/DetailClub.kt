package com.raivstudio.footballclub

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.View
import org.jetbrains.anko.*

class DetailClub : AppCompatActivity() {

    companion object {
        val Posisi = "posisi"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val _intent = intent
        val paket = _intent.getParcelableExtra<ItemClub>(Posisi)
        tampilanDetail(paket).setContentView(this)
    }

    class tampilanDetail(var isi : ItemClub) : AnkoComponent<DetailClub>{
        override fun createView(ui: AnkoContext<DetailClub>) = with (ui) {
            verticalLayout(){
                lparams(matchParent, matchParent){
                    gravity = Gravity.CENTER
                }
                imageView(){
                    setImageResource(isi.image!!)
                }.lparams(75, 75)
                textView(){
                    text = isi.name
                    textAlignment = View.TEXT_ALIGNMENT_CENTER
                    padding = dip(15)
                }
                textView(){
                    text = isi.desc
                    textAlignment = View.TEXT_ALIGNMENT_CENTER
                }
            }
        }

    }
}
