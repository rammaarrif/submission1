package com.raivstudio.footballclub

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemClub (val name: String?, val image: Int?, val desc:String?) : Parcelable