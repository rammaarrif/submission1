package com.raivstudio.footballclub

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import org.jetbrains.anko.startActivity

class MainActivity : AppCompatActivity() {

    private var items: MutableList<ItemClub> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val list = findViewById<RecyclerView>(R.id.list_club)
        initData()

        list.layoutManager = LinearLayoutManager(this)
        list.adapter = ClubAdapter(this, items){
            startActivity<DetailClub>(DetailClub.Posisi to it)
        }
    }

    private fun initData(){
        val name    = resources.getStringArray(R.array.club_name)
        val image   = resources.obtainTypedArray(R.array.club_image)
        val desc    = resources.getStringArray(R.array.club_desc)

        items.clear()

        for (i in name.indices) {
            items.add(ItemClub(name[i], image.getResourceId(i, 0), desc[i]))
        }
        image.recycle()
    }
}


